<?php


namespace SocialiteProviders\MyCHS;

use SocialiteProviders\Manager\SocialiteWasCalled;

class MyCHSExtendSocialite {

    public function handle(SocialiteWasCalled $socialiteWasCalled) {
        $socialiteWasCalled->extendSocialite('mychs',__NAMESPACE__.'\Provider');
    }
}