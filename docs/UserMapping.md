# Map Users between MyCHS and BookStack

After completing the OAuth2 handshake, the calling application (BookStack) performs an (then) authorized request to fetch the MyCHS-ID of the authenticated user. 

If the authenticated user is within the Chairos company, the callback for the user ID will return the actual user ID. If the user is identified from another company than Chairos (e.g. customer), MyCHS returns a virtual user id dependend of the user's main group setting.

### BookStack: the `social_accounts` table
The MyCHS-ID of the user is then mapped against the `driver_id` column in the `social_accounts` table of your _BookStack_ installation.

|id|user_id|driver|driver_id|avatar|created_at         |updated_at         |
|--|-------|------|---------|------|-------------------|-------------------|
| 3|      4| mychs|     2387|      |2019-02-15 16:45:02|2019-02-15 16:45:02|

### MyCHS: the `groups` table

In the table `groups` in MyCHS the column `oauth_user_id` contains a (virtual) user ID for each group.

#### Mapping

To map non-Chairos users to BookStack accounts add an entry to the `social_accounts` table of BookStack which contains the `oauth_user_id` of the MyCHS groups table as `driver_id` of the `social_accounts` table. 

So in order to map the `oauth_user_id` of "9090" of a given MyCHS user group to the BookStack user with the ID of 8:

```
insert into social_accounts (user_id, driver, driver_id, avatar) values (8, 'mychs', 9090, '');
```