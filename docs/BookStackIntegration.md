# BookStack integration

Authenticating BookStack users via MyCHS was the reason for development of this [Socialite plugin](http://socialiteproviders.github.io/)
in the first place. You may use `socialte-mychs` for any (Laravel) application authenticating against MyCHS.

### Prerequisites

You'll need running instances of _BookStack_ and _MyCHS_ for this to work (d'oh!).

The URLs of your _MyCHS_ and _BookStack_ installations are referred as:
- **BOOKSTACK_URI**: your BookStack installation (e.g. `https://bookstack.company.com`)
- **MYCHS_URI**: location of your MyCHS installation (e.g. `https://my-chs.company.com`)


#### MyCHS
-  visit `MYCHS_URI/oauth/applications` of your MyCHS installation.
(you need `csc_admin` privileges for this), and add / edit these values:
    - **Name**: will be used to ask users for permission ("You want $Name to access your account?") 
    - **Redirect URI**: `BOOKSTACK_URI/login/service/mychs/callback`
    - **Scopes**: leave this empty

- after submitting the form write down the __app id__ and __secret__ 

#### BockStack
In your BookStack installation you'll need to add the following environment variables.
BookStack uses an [.env file](https://github.com/BookStackApp/BookStack/blob/master/.env.example)
for setting the environment at runtime. 

Add these lines to the `.env` file:

```
# MyCHS
MYCHS_APP_ID=<the app id>
MYCHS_APP_SECRET=<the secret>
MYCHS_BASE_URI=<MYCHS_URI>
```

and set
```
APP_URL=<BOOKSTACK_URI>
```
which usually can be found at the begnning of the file

#### Authentication with Bookstack
This socialite plugin currently isn't part of the official BookStack release (and won't be in the future).
Documentation how to add `socialite-mychs` to your official BookStack release may follow. 
In the meantime use the [unofficial distribution](https://gitlab.com/chairos/BookStack/-/archive/dist/my-chs-oauth/BookStack-dist-my-chs-oauth.zip).
